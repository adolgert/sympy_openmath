"""
sympy/parsing/openmath.py
Parses OpenMath XML encoding.
"""
import os
import logging
import io
import struct
import sympy
from sympy import sympify
from sympy.core import S
from sympy import ccode
import lxml.etree
import ply.yacc
import ply.lex

logger=logging.getLogger(__file__)


def open_math(s):
    return sympify(parse(s))


def parse(s, extra_lookup=None):
    """
    s is a string. extra_lookup is an optional table from
    OpenMath cd to sympy symbol. It is a dictionary just like
    _lookup_table below.
    """
    return parse_etree(lxml.etree.parse(io.StringIO(s)), extra_lookup)


OM_DIR = 'om'

def validator():
    relaxng_doc = lxml.etree.parse(os.path.join(OM_DIR,'openmath2.rng'))
    relaxng = lxml.etree.RelaxNG(relaxng_doc)
    return relaxng


def validate(etree_doc, relax_ng):
    is_valid=relax_ng.validate(etree_doc)
    logger.debug('valid? {0}'.format(is_valid))
    return is_valid

bare_tags=['omobj', 'oms', 'oma', 'omi', 'omv', 'omf', 'omattr',
    'ombind', 'ombvar']
ns='http://www.openmath.org/OpenMath'
tags={t : '{{{0}}}{1}'.format(ns,t.upper()) for t in bare_tags}
rev_tags={v : t for t,v in tags.items()}

_lookup_table={
    # alg1
    'http://www.openmath.org/cd/alg1#one' : S.One,
    'http://www.openmath.org/cd/alg1#zero' : S.Zero,
    # arith1
    'http://www.openmath.org/cd/arith1#abs' : sympy.Abs,
    'http://www.openmath.org/cd/arith1#divide' : \
      lambda x, y : sympy.Mul(x,sympy.Pow(y,sympy.Integer(-1))),
    'http://www.openmath.org/cd/arith1#gcd' : None,
    'http://www.openmath.org/cd/arith1#lcm' : None,
    'http://www.openmath.org/cd/arith1#minus' : \
      lambda x, y : sympy.Add(x, sympy.Mul(y, sympy.Integer(-1))),
    'http://www.openmath.org/cd/arith1#plus' : sympy.Add,
    'http://www.openmath.org/cd/arith1#power' : sympy.Pow,
    'http://www.openmath.org/cd/arith1#product' : \
      lambda prod_range, expr : sympy.Product(expr, prod_range),
    'http://www.openmath.org/cd/arith1#root' : sympy.root,
    'http://www.openmath.org/cd/arith1#sum' : \
      lambda sum_range, expr : sympy.Sum(expr, sum_range),
    'http://www.openmath.org/cd/arith1#times' : sympy.Mul,
    'http://www.openmath.org/cd/arith1#unary_minus' : \
      lambda x : sympy.Mul(x, sympy.Integer(-1)),
    # complex1
    'http://www.openmath.org/cd/complex1#argument' : sympy.arg,
    'http://www.openmath.org/cd/complex1#complex_cartesian' : None,
    'http://www.openmath.org/cd/complex1#complex_polar' : None,
    'http://www.openmath.org/cd/complex1#conjugate' : sympy.functions.conjugate,
    'http://www.openmath.org/cd/complex1#imaginary' : sympy.im,
    'http://www.openmath.org/cd/complex1#real' : sympy.re,
    # fns1
    'http://www.openmath.org/cd/fns1#lambda' : sympy.Lambda,
    # minmax1
    'http://www.openmath.org/cd/minmax1#max' : sympy.Max,
    'http://www.openmath.org/cd/minmax1#min' : sympy.Min,
    # nums1
    'http://www.openmath.org/cd/nums1#NaN' : S.NaN,
    'http://www.openmath.org/cd/nums1#based_float' : None,
    'http://www.openmath.org/cd/nums1#based_integer' : None,
    'http://www.openmath.org/cd/nums1#e' : sympy.E,
    'http://www.openmath.org/cd/nums1#gamma' : sympy.gamma,
    'http://www.openmath.org/cd/nums1#i' : sympy.I,
    'http://www.openmath.org/cd/nums1#infinity' : S.Infinity,
    'http://www.openmath.org/cd/nums1#pi' : sympy.pi,
    'http://www.openmath.org/cd/nums1#rational' : sympy.Rational,
    # relation1
    'http://www.openmath.org/cd/relation1#approx' : None,
    'http://www.openmath.org/cd/relation1#eq' : sympy.Eq,
    'http://www.openmath.org/cd/relation1#geq' : sympy.GreaterThan,
    'http://www.openmath.org/cd/relation1#gt' : sympy.StrictGreaterThan,
    'http://www.openmath.org/cd/relation1#leq' : sympy.LessThan,
    'http://www.openmath.org/cd/relation1#lt' : sympy.StrictLessThan,
    'http://www.openmath.org/cd/relation1#neq' : sympy.Ne,
    # rounding1
    'http://www.openmath.org/cd/rounding1#ceiling' : sympy.ceiling,
    'http://www.openmath.org/cd/rounding1#floor' : sympy.floor,
    'http://www.openmath.org/cd/rounding1#round' : None,
    'http://www.openmath.org/cd/rounding1#trunc' : None,
    # transc1
    'http://www.openmath.org/cd/transc1#arccos' : sympy.acos,
    'http://www.openmath.org/cd/transc1#arccosh' : sympy.acosh,
    'http://www.openmath.org/cd/transc1#arccot' : sympy.acot,
    'http://www.openmath.org/cd/transc1#arccosh' : sympy.acoth,
    'http://www.openmath.org/cd/transc1#arccsc' : None,
    'http://www.openmath.org/cd/transc1#arcsch' : None,
    'http://www.openmath.org/cd/transc1#arcsec' : None,
    'http://www.openmath.org/cd/transc1#arcsech' : None,
    'http://www.openmath.org/cd/transc1#arcsin' : sympy.asin,
    'http://www.openmath.org/cd/transc1#arcsinh' : sympy.asinh,
    'http://www.openmath.org/cd/transc1#arctan' : sympy.atan,
    'http://www.openmath.org/cd/transc1#arctanh' : sympy.atanh,
    'http://www.openmath.org/cd/transc1#cos' : sympy.cos,
    'http://www.openmath.org/cd/transc1#cosh' : sympy.cosh,
    'http://www.openmath.org/cd/transc1#cot' : sympy.cot,
    'http://www.openmath.org/cd/transc1#coth' : sympy.coth,
    'http://www.openmath.org/cd/transc1#csc' : None,
    'http://www.openmath.org/cd/transc1#csch' : None,
    'http://www.openmath.org/cd/transc1#exp' : sympy.exp,
    'http://www.openmath.org/cd/transc1#ln' : sympy.log,
    'http://www.openmath.org/cd/transc1#log' : \
        lambda base, x: sympy.log(x)/sympy.log(base),
    'http://www.openmath.org/cd/transc1#sec' : sympy.sec,
    'http://www.openmath.org/cd/transc1#sech' : None,
    'http://www.openmath.org/cd/transc1#sin' : sympy.sin,
    'http://www.openmath.org/cd/transc1#sinh' : sympy.sinh,
    'http://www.openmath.org/cd/transc1#tan' : sympy.tan,
    'http://www.openmath.org/cd/transc1#tanh' : sympy.tanh,
}


def _lookup(symbol, extra_lookup):
    if extra_lookup and symbol in extra_lookup:
        return extra_lookup[symbol]
    else:
        if symbol in _lookup_table:
            return _lookup_table[symbol]
        else:
            return None



class XmlTokenizer(object):
    def __init__(self, xml_file):
        self.lexpos=0
        events = ("start", "end")
        self.context = lxml.etree.iterparse(xml_file, events=events)

    def token(self):
        try:
            action, elem=self.context.__next__()
        except StopIteration:
            logger.debug('Tokenizer returning None.')
            return None
        token=ply.lex.LexToken()
        if action=='start':
            token.value=(elem.tag, elem.attrib, elem.text)
        else:
            token.value=elem.tag
        token.lineno=elem.sourceline
        token.lexpos=self.lexpos
        token.type="_".join([rev_tags[elem.tag], action])
        token.lexer=None
        self.lexpos+=1
        logger.debug('Tokenizer returning {0}'.format(token))
        return token


cdbase_stack=list()


def cdstack_push(p):
    if 'cdbase' in p[1][1]:
        logger.debug('Push cdbase {0}'.format(p[1][1]['cdbase']))
        cdbase_stack.append(p[1][1]['cdbase'])
    p[0]=p[1]



def cdstack_pop(attrib):
    if 'cdbase' in attrib:
        cdbase_stack.pop()


def open_math_parser(extra_lookup):
    tokens=['{0}_start'.format(x) for x in bare_tags]
    tokens.extend(['{0}_end'.format(x) for x in bare_tags])
    logger.debug(tokens)

    def p_expression_omobj(p):
        'omobj : omobj_pull omobj omobj_end'
        p[0]=p[2]

    # basic objects are omobjects
    def p_omobj_basic(p):
        'omobj : basic'
        p[0]=p[1]

    # compound objects are omobjects
    def p_omobj_compound(p):
        'omobj : compound'
        p[0]=p[1]

    # list of objects.
    def p_objlist_omobj(p):
        'objlist : omobj omobj'
        p[0]=(p[1], p[2])

    # append to the list
    def p_objlist_append(p):
        'objlist : objlist omobj'
        p[0]=p[1]+(p[2],)

    # attribution list
    def p_attriblist(p):
        'attriblist : attribution'
        p[0]=p[1]

    # append to attribution list
    def p_attriblist_append(p):
        'attriblist : attriblist attribution'
        p[0]=p[1]+(p[2],)

    # list of variables
    def p_varlist(p):
        'varlist : omv'
        p[0]=p[1]

    def p_varlist_append(p):
        'varlist : varlist omv'
        p[0]=p[1]+(p[2],)

    # atrribution
    def p_attribitem(p):
        'attribution : oms omobj'
        p[0]=(p[1], p[2])

    # These are all basic objects.
    def p_basic_oms(p):
        'basic : oms'
        p[0]=p[1]

    def p_basic_omi(p):
        'basic : omi'
        p[0]=p[1]

    def p_basic_omf(p):
        'basic : omf'
        p[0]=p[1]

    def p_basic_omv(p):
        'basic : omv'
        p[0]=p[1]


    # Which objects are the compound objects
    def p_compound_oma(p):
        'compound : oma'
        p[0]=p[1]

    def p_compound_omattr(p):
        'compound : omattr'
        p[0]=p[1]


    def p_compound_ombind(p):
        'compound : ombind'
        p[0]=p[1]


    def p_omattr_none(p):
        'omattr : omattr_pull omobj omattr_end'
        p[0]=p[2]


    # This throws away the attribution list.
    def p_omattr_none(p):
        'omattr : omattr_pull omobj attriblist omattr_end'
        p[0]=p[2]


    # An application of a function to no arguments
    def p_oma_omobj(p):
        'oma : oma_pull omobj oma_end'
        p[0] = p[1]()
        cdstack_pop(p[1][1])


    # An application of a function to a set of arguments.
    def p_oma_objlist(p):
        'oma : oma_pull objlist oma_end'
        logger.debug('oma_objlist {0}'.format(p[2][0]))
        p[0] = p[2][0](*p[2][1:])
        cdstack_pop(p[1][1])


    def p_ombind_full(p):
        'ombind : ombind_pull omobj ombvar omobj ombind_end'
        p[0]=p[2](p[3], p[4])


    def p_ombvar_varlist(p):
        'ombvar : ombvar_pull varlist ombvar_end'
        p[0]=p[2]


    # Look up a symbol.
    def p_oms_omspull(p):
        'oms : oms_pull oms_end'
        if cdbase_stack:
            symbol='{0}/{1}#{2}'.format(
                    cdbase_stack[-1], p[1][1]['cd'], p[1][1]['name'])
        else:
            symbol='{0}#{1}'.format(p[1][1]['cd'], p[1][1]['name'])

        sympy_symbol=_lookup(symbol, extra_lookup)
        if sympy_symbol:
            p[0] = sympy_symbol
        else:
            logger.error('Cannot find translation for {0}'.format(symbol))
        cdstack_pop(p[1][1])


    def p_expression_omi(p):
        'omi : omi_pull omi_end'
        p[0]=sympy.Integer(p[1][2])


    def p_expression_omf(p):
        'omf : omf_pull omf_end'
        if 'dec' in p[1][1]:
            val=p[1][1]['dec']
            if val=='INF':
                p[0]=S.Infinity
            elif val=='-INF':
                p[0]=S.NegativeInfinity
            elif val=='NaN':
                p[0]=S.NaN
            else:
                p[0]=sympy.Float(val)
        elif 'hex' in p[1][1]:
            # IEEE float
            p[0]=sympy.Float(struct.unpack("d", p[1][1]['hex'])[0])
        else:
            raise RuntimeError('cannot unpack {0}'.format(p))


    def p_omv_start(p):
        'omv : omv_pull omv_end'
        p[0] = sympy.symbols(p[1][1]['name'])


    def p_expression_omapull(p):
        'oma_pull : oma_start'
        cdstack_push(p)


    def p_expression_omspull(p):
        'oms_pull : oms_start'
        cdstack_push(p)


    def p_expression_omipull(p):
        'omi_pull : omi_start'
        cdstack_push(p)


    def p_expression_omfpull(p):
        'omf_pull : omf_start'
        cdstack_push(p)


    def p_expression_omvpull(p):
        'omv_pull : omv_start'
        cdstack_push(p)


    def p_expression_omattrpull(p):
        'omattr_pull : omattr_start'
        cdstack_push(p)


    def p_expression_ombindpull(p):
        'ombind_pull : ombind_start'
        cdstack_push(p)


    def p_expression_ombvarpull(p):
        'ombvar_pull : ombvar_start'
        cdstack_push(p)


    def p_expression_omobjpull(p):
        'omobj_pull : omobj_start'
        cdstack_push(p)


    def p_error(p):
        if p is None:
            # End of file condition
            logger.debug('End of file')
        else:
            print("Syntax error in input! {0}".format(p))


    parser=ply.yacc.yacc()
    return parser



def parse_etree(equation_etree, extra_lookup=None):
    xml_lexer=XmlTokenizer(equation_etree)
    parser=open_math_parser(extra_lookup)
    result=parser.parse(lexer=xml_lexer)
    logger.debug(result)
    return result


XMLDIR = 'example'

def example_file(example_name):
    return os.path.join(XMLDIR,'{0}.xml'.format(example_name))

def example(example_name):
    doc = lxml.etree.parse(example_file(example_name))
    return doc


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    extra={
    'http://www.openmath.org/cd/transc1#csc' : \
        lambda x: 1/sympy.cos(x)
    }
    example_name='bind'
    validate(example(example_name), validator())
    result=parse_etree(example_file(example_name), extra)
    print(sympy.ccode(result))
