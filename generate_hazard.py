import os
import logging
import sympy
import lxml
import openmath
from default_parser import DefaultArgumentParser


logger=logging.getLogger(__file__)


def expression_symbols(expression):
	"""
	Return a list of all symbols in the expression.
	These are the variables, in common parlance.
	Convert all symbols that aren't t into args[t].
	"""
	if isinstance(expression, sympy.Symbol):
		logger.debug('found {0}'.format(expression))
		if expression.name!='t':
			expression.name='args["{0}"]'.format(expression.name)
		return [expression]
	else:
		variables=list()
		for arg in expression.args:
			logger.debug('examining arg {0}'.format(arg))
			res=expression_symbols(arg)
			if res:
				variables.extend(res)
		if variables:
			return variables
	return




def encode_equation(equation_filename):
	just_file=os.path.basename(equation_filename)
	function_name='.'.join(just_file.split('.')[0:-1])
	logger.debug('opening filename {0}'.format(equation_filename))
	sympy_expression=openmath.parse_etree(equation_filename)
	all_symbols=expression_symbols(sympy_expression)
	head="""
double {funcname}(double t, std::map<int,double> args) {{
	return {body};	
}}
""".format(funcname=function_name, body=sympy.ccode(sympy_expression))
	return head



if __name__ == '__main__':
	parser=DefaultArgumentParser(description="""
This reads an OpenMath file in XML encoding and produces a C++
function that evaluates that equation.
"""
		)
	parser.add_function('encode',
		'Convert an OpenMath XML file to a C function.')
	parser.add_argument('--om', metavar='omfile', type=str,
			help='OpenMath equation in XML encoding')
	args=parser.parse_args()
	if args.encode:
		print(encode_equation(args.om))
