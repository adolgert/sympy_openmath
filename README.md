OpenMath for Sympy
====================
This reads mathematical equations encoded with [OpenMath](http://www.openmath.org/) into the Python symbolic math package [sympy](http://sympy.org/en/index.html). Depends on [lxml](http://lxml.de/) and [ply](http://www.dabeaz.com/ply/).


When you download it, it will need some files in a subdirectory called "om", for Open Math. Go to the page for the [OpenMath Standard 2.0](OpenMath Standard 2.0" target="http://www.openmath.org/standard/om20-2004-06-30/), and download the files that end in ".rng".
